/// @description Init the Character

run_speed = 8;
roll_speed = 8;
dash_speed = 20;
defensive_speed = 4;

health_points = 100;
mana_points = 100;
stamina_points = 100;

experience_points = 0;
level = 1;

is_hurt = false;
is_rolling = false;
is_dashing = false;
is_blocking = false;

// State arrays
enum character_state {
	move,
	dash,
	roll,
	block,
	hurt
}

enum facing {
	right = 0,
	up = 1,
	left = 2,
	down = 3
}
 
state = character_state.move;
direction_facing = facing.right;

// Sprite Lookup Array
sprite_lookup[character_state.move, facing.right] = spr_character_walk_right;
sprite_lookup[character_state.move, facing.up] = spr_character_walk_up;
sprite_lookup[character_state.move, facing.left] = spr_character_walk_left;
sprite_lookup[character_state.move, facing.down] = spr_character_walk_down;

/*
NYI
sprite_lookup[character_state.dash, facing.right] = spr_character_dash_right;
sprite_lookup[character_state.dash, facing.up] = spr_character_dash_up;
sprite_lookup[character_state.dash, facing.left] = spr_character_dash_right;
sprite_lookup[character_state.dash, facing.down] = spr_character_dash_down;
 
sprite_lookup[character_state.roll, facing.right] = spr_character_roll_right;
sprite_lookup[character_state.roll, facing.up] = spr_character_roll_up;
sprite_lookup[character_state.roll, facing.left] = spr_character_roll_right;
sprite_lookup[character_state.roll, facing.down] = spr_character_roll_down;
 
sprite_lookup[character_state.block, facing.right] = spr_character_block_right;
sprite_lookup[character_state.block, facing.up] = spr_character_block_up;
sprite_lookup[character_state.block, facing.left] = spr_character_block_right;
sprite_lookup[character_state.block, facing.down] = spr_character_block_down;

sprite_lookup[character_state.hurt, facing.right] = spr_character_hurt_right;
sprite_lookup[character_state.hurt, facing.up] = spr_character_hurt_up;
sprite_lookup[character_state.hurt, facing.left] = spr_character_hurt_right;
sprite_lookup[character_state.hurt, facing.down] = spr_character_hurt_down;
*/