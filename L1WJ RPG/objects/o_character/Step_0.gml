/// @description 

image_speed = 0;
var animation_speed = 0.5;

// Get user input
var x_input = (o_input.right_input - o_input.left_input);
var y_input = (o_input.down_input - o_input.up_input);
var input_direction = point_direction(0, 0, x_input, y_input);


if (x_input == 0 and y_input == 0){
	image_index = 0;	
} else {
	direction_facing = get_direction_facing(input_direction);
}

move_me(x_input, y_input, run_speed, o_solid);

sprite_index = sprite_lookup[character_state.move, direction_facing];
