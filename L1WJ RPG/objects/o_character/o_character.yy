{
    "id": "c0646408-711c-49f8-b0aa-b458ac92caf5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_character",
    "eventList": [
        {
            "id": "46a3e772-9471-44b7-9693-3d5b400a9af9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0646408-711c-49f8-b0aa-b458ac92caf5"
        },
        {
            "id": "4163b7eb-ef04-4b80-a655-8bc40b701a7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0646408-711c-49f8-b0aa-b458ac92caf5"
        }
    ],
    "maskSpriteId": "dec7baeb-74cc-4efc-b64a-ac6c75538dff",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "89cf8571-b732-4892-8bf9-a2fb18fa613c",
    "visible": true
}