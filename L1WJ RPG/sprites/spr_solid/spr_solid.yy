{
    "id": "84dbf25c-3d0a-4788-a9cc-cf51569e0364",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ef95771-80e0-4edb-8470-3a8647f0a33b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84dbf25c-3d0a-4788-a9cc-cf51569e0364",
            "compositeImage": {
                "id": "35319968-a7f4-41aa-9f29-f978f3b9a780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef95771-80e0-4edb-8470-3a8647f0a33b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b63e0c-0981-48f4-88b5-22b6117cdc7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef95771-80e0-4edb-8470-3a8647f0a33b",
                    "LayerId": "7f4c7cdd-f311-4e4a-aa90-2d2846ebfd99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f4c7cdd-f311-4e4a-aa90-2d2846ebfd99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84dbf25c-3d0a-4788-a9cc-cf51569e0364",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}