{
    "id": "89cf8571-b732-4892-8bf9-a2fb18fa613c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b48fb976-0dbe-4bc2-809c-fbcead21ece8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89cf8571-b732-4892-8bf9-a2fb18fa613c",
            "compositeImage": {
                "id": "65d369d3-606c-4a58-9134-2e840a8bc7de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48fb976-0dbe-4bc2-809c-fbcead21ece8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd38f279-4b6e-4164-93e1-8ff66c7e3a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48fb976-0dbe-4bc2-809c-fbcead21ece8",
                    "LayerId": "f648ac10-fd49-4587-8b32-334af4a33586"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f648ac10-fd49-4587-8b32-334af4a33586",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89cf8571-b732-4892-8bf9-a2fb18fa613c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}