{
    "id": "dd8d211d-2701-4883-870e-8eb367686a6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8e8c434-174d-4bf6-9f87-12e8fb127838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd8d211d-2701-4883-870e-8eb367686a6a",
            "compositeImage": {
                "id": "9311788a-4ed8-4621-aa02-8c4def459e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e8c434-174d-4bf6-9f87-12e8fb127838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5334bee-e66a-4dec-bb94-6b4146faa6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e8c434-174d-4bf6-9f87-12e8fb127838",
                    "LayerId": "8ff26ec6-8c65-4465-b202-bc278528e5b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ff26ec6-8c65-4465-b202-bc278528e5b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd8d211d-2701-4883-870e-8eb367686a6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}