{
    "id": "d610df06-0e89-456c-be75-b9d677c766d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8257319-ea93-42ff-a29b-56fb54364ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d610df06-0e89-456c-be75-b9d677c766d7",
            "compositeImage": {
                "id": "6837d9bf-6799-4486-a9aa-be271506ef35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8257319-ea93-42ff-a29b-56fb54364ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a0066ce-9b4f-4548-a209-8fdb693324e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8257319-ea93-42ff-a29b-56fb54364ae0",
                    "LayerId": "014c983b-434c-421a-b7a0-b277d09537b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "014c983b-434c-421a-b7a0-b277d09537b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d610df06-0e89-456c-be75-b9d677c766d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}