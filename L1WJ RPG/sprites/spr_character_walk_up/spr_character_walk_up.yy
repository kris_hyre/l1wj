{
    "id": "e9f96ff2-29f0-4505-a3dc-a7f30b9c30e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e615758-9994-4ca6-ac0a-7f3126e4ee2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f96ff2-29f0-4505-a3dc-a7f30b9c30e8",
            "compositeImage": {
                "id": "b5511cf9-21de-43ca-b615-54487ebb44c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e615758-9994-4ca6-ac0a-7f3126e4ee2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebe0aa9b-59e8-47b7-9fa5-e910625b4885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e615758-9994-4ca6-ac0a-7f3126e4ee2f",
                    "LayerId": "c519af70-0832-4330-b9c6-ed9f86bfdf94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c519af70-0832-4330-b9c6-ed9f86bfdf94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9f96ff2-29f0-4505-a3dc-a7f30b9c30e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}