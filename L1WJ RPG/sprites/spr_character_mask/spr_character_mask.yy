{
    "id": "dec7baeb-74cc-4efc-b64a-ac6c75538dff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 5,
    "bbox_right": 59,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d64aa798-3e85-460e-9322-ac5bbf795643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dec7baeb-74cc-4efc-b64a-ac6c75538dff",
            "compositeImage": {
                "id": "06e0a8c1-e135-4aa9-a4da-2907880cdac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d64aa798-3e85-460e-9322-ac5bbf795643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9ee103-38f6-4e86-8818-7ae96fd00239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d64aa798-3e85-460e-9322-ac5bbf795643",
                    "LayerId": "abf37ec9-ea54-4aa5-aed3-20938d22e090"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "abf37ec9-ea54-4aa5-aed3-20938d22e090",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dec7baeb-74cc-4efc-b64a-ac6c75538dff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}