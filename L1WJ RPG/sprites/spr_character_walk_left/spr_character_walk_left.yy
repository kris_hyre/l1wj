{
    "id": "6a4aa920-41ed-4cc7-8bad-60ee47878ea5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8e0cd50-eadb-4773-acc0-6e5b86e97e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a4aa920-41ed-4cc7-8bad-60ee47878ea5",
            "compositeImage": {
                "id": "889c7f5f-513d-4c23-97c6-081dfb5cf5bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8e0cd50-eadb-4773-acc0-6e5b86e97e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ab0cebb-9170-4abc-8637-9d546b8429ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8e0cd50-eadb-4773-acc0-6e5b86e97e40",
                    "LayerId": "2e2de801-5799-4527-831c-eb45b659a6d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e2de801-5799-4527-831c-eb45b659a6d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a4aa920-41ed-4cc7-8bad-60ee47878ea5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}