{
    "id": "74f2ec4e-3110-42b0-9939-a25e7bf07308",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_home_city",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "686e53ea-ccf3-4e06-9616-69242b9bacfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74f2ec4e-3110-42b0-9939-a25e7bf07308",
            "compositeImage": {
                "id": "4f765ae4-9efa-41a2-a8cb-ba8737ee0288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "686e53ea-ccf3-4e06-9616-69242b9bacfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae6a85d7-d4b9-4506-bce1-98539fdc7c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "686e53ea-ccf3-4e06-9616-69242b9bacfd",
                    "LayerId": "34eb4e6e-daf5-46f0-b017-db8dddbce0a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "34eb4e6e-daf5-46f0-b017-db8dddbce0a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74f2ec4e-3110-42b0-9939-a25e7bf07308",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}