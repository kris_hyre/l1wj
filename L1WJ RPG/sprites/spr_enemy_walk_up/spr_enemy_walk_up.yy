{
    "id": "f3a51e45-9609-41fc-8ee9-b465879e4ce4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66707351-df4d-4dae-b6e2-9721928ecaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3a51e45-9609-41fc-8ee9-b465879e4ce4",
            "compositeImage": {
                "id": "589459a5-f2c0-4b00-9295-e971fbfdceab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66707351-df4d-4dae-b6e2-9721928ecaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf6c0bf-b930-40a8-9480-2948fb5f6e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66707351-df4d-4dae-b6e2-9721928ecaf2",
                    "LayerId": "bea0a6a0-645f-4561-b2f4-1ba5ed0694ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bea0a6a0-645f-4561-b2f4-1ba5ed0694ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3a51e45-9609-41fc-8ee9-b465879e4ce4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}