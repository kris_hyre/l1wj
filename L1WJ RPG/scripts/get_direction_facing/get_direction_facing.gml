/// @param input_direction
// Determine the direction facing by the input from the controls, if 4 set to 0, used by facing array

var input_dir = argument0
var dir_value = round(input_dir / 90);

if (dir_value == 4){ 
	dir_value = 0;
}

switch (dir_value){
	case 0: 
		return facing.right;
		break;
	case 1:
		return facing.up;
		break;
	case 2: 
		return facing.left;
		break;
	case 3:
		return facing.down;
		break;
	default:
		return facing.right;
		break;
}