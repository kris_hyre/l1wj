/// @param x_input
/// @param y_input
/// @param movement_speed
/// @param collision_object

var x_input = argument0;
var y_input = argument1;
var move_speed = argument2;
var col_object = argument3;

var h_speed = x_input * move_speed;
var v_speed = y_input * move_speed;

if (!place_meeting(x + h_speed, y, col_object)){
	self.x += h_speed;	
}

if (!place_meeting(x, y + v_speed, col_object)){
	self.y += v_speed;	
}

